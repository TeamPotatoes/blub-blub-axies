

import type { Handler, HandlerEvent, HandlerContext } from "@netlify/functions";
import axios from 'axios';

const handler: Handler = async (event: HandlerEvent, context: HandlerContext) => {
  const apiKey = process.env.API_KEY || ''
  const api = axios.create({
    baseURL: 'https://api-gateway.skymavis.com/graphql/',
    headers: { 'X-Api-Key': apiKey }
  });
  const axieQuery = '<_alias>: axie(axieId: \"<_id>\") {\n    id\n    image\n    parts {\n      name\n      id\n    }\n  }';
  const axieMarketplaceQuery = '<_alias>: axies(\n    auctionType: Sale\n    criteria: {parts: <_part>}\n    sort: PriceAsc\n ) {\n    results {\n      order {\n        currentPrice\n      }\n    }\n    total\n  }'
  let body = JSON.parse(event.body)
  const axieIDs = body.id
  let queryBuilder = ''
  axieIDs.forEach(id => {
    let query = axieQuery.slice()
    query = query.replace('<_id>', id)
    query = query.replace('<_alias>', `axie_${id}`)
    query += ','
    queryBuilder += query
  })
  queryBuilder = queryBuilder.slice(0, -1)

  let axieResponse = await api.post('axie-marketplace', { "query": `query MyQuery {\n  ${queryBuilder}}\n`, "operationName": "MyQuery" })

  if (axieResponse.status === 200) {
    let parts = []
    for (let [key, value] of Object.entries(axieResponse.data.data)) {
      let object = {}
      object[`axie_${value.id}`] = value.parts.map(part => part.id)
      parts.push(object)
    }
    queryBuilder = ''
    parts.forEach(axie => {
      for (let [key, value] of Object.entries(axie)) {
        let query = axieMarketplaceQuery.slice()
        query = query.replace('<_part>', JSON.stringify(value))
        query = query.replace('<_alias>', `${key}`)
        query += ','
        queryBuilder += query
      }
    })
    queryBuilder = queryBuilder.slice(0, -1)
    let marketResponse = await api.post('axie-marketplace', { "query": `query MyQuery {\n  ${queryBuilder}}\n`, "operationName": "MyQuery" })
    if (marketResponse.status === 200) {
      for (let [key, value] of Object.entries(marketResponse.data.data)) {
        value['image'] = axieResponse.data.data[key]?.image
        value['id'] = key.split('_')[1]
      }
      return {
        statusCode: 200,
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ data: marketResponse.data.data }),
      };
    }
  }


};

export { handler };