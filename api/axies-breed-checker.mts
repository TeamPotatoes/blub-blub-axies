

import type { Handler, HandlerEvent, HandlerContext } from "@netlify/functions";
import axios from 'axios';

/**
 * Momentos IDs
 * Beast : 1099511627776
 * Bug: 1103806595072
 * Bird: 1108101562368
 * Plant: 1112396529664
 * Aquatic: 1116691496960
 * Reptile: 1120986464256
 * Mech: 1168231104512
 * Dawn: 1172526071808
 * Dusk: 1176821039104
 */

const handler: Handler = async (event: HandlerEvent, context: HandlerContext) => {
  const apiKey = process.env.API_KEY || ''
  const api = axios.create({
    baseURL: 'https://api-gateway.skymavis.com/graphql/',
    headers: { 'X-Api-Key': apiKey }
  });
  const axieQuery = 'query MyQuery($breedCount: [Int!] = [4, 7]) {\n  reptiles: axies(\n    auctionType: Sale\n    criteria: {breedCount: $breedCount, classes: [Reptile], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  bugs: axies(\n    auctionType: Sale\n    criteria: {breedCount: $breedCount, classes: [Bug], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  beasts: axies(\n    auctionType: Sale\n    criteria: {breedCount: $breedCount, classes: [Beast], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  aquas: axies(\n    auctionType: Sale\n    criteria: {breedCount: $breedCount, classes: [Aquatic], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  birds: axies(\n    auctionType: Sale\n    criteria: {breedCount: $breedCount, classes: [Bird], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  plants: axies(\n    auctionType: Sale\n    criteria: {breedCount: $breedCount, classes: [Plant], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  reptilesbc3: axies(\n    auctionType: Sale\n    criteria: {breedCount: 3, classes: [Reptile], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  bugsbc3: axies(\n    auctionType: Sale\n    criteria: {breedCount: 3, classes: [Bug], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  beastsbc3: axies(\n    auctionType: Sale\n    criteria: {breedCount: 3, classes: [Beast], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  aquasbc3: axies(\n    auctionType: Sale\n    criteria: {breedCount: 3, classes: [Aquatic], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  birdsbc3: axies(\n    auctionType: Sale\n    criteria: {breedCount: 3, classes: [Bird], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  plantsbc3: axies(\n    auctionType: Sale\n    criteria: {breedCount: 3, classes: [Plant], pureness: 6, purity: [60, 100]}\n    size: 8\n    from: 0\n    sort: PriceAsc\n  ) {\n    ...axieData\n  }\n  momentos: erc1155Tokens(\n    tokenType: Material\n    tokenIds: ["1099511627776", "1103806595072", "1108101562368", "1112396529664", "1116691496960", "1120986464256"]\n  ) {\n    results {\n      minPrice\n      name\n      tokenId\n      tokenType\n      orders(sort: PriceAsc, size: 1, from: 0) {\n        data {\n          currentPriceUsd\n          currentPrice\n        }\n      }\n    }\n  }\n}\n\nfragment axieData on Axies {\n  results {\n    breedCount\n    id\n    image\n    order {\n      currentPrice\n      currentPriceUsd\n    }\n    axpInfo {\n      level\n      xp\n      xpToLevelUp\n    }\n  }\n  total\n}';

  let axieResponse = await api.post('axie-marketplace', { "query": `${axieQuery}`, "operationName": "MyQuery" })

  if (axieResponse.status === 200) {
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ data: axieResponse.data.data }),
    };
  }


};

export { handler };